import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import MealItem from './MealItem';

export default function MealList(props) {

    const renderMealItem = itemData => {
        return (
            <MealItem
                title={itemData.item.title}
                image={itemData.item.imageUrl}
                duration={itemData.item.duration}
                complexity={itemData.item.complexity}
                affordability={itemData.item.affordability}
                onSelectMeal={()=>{
                    props.navigation.navigate(
                        {
                            routeName: 'MealDetail',
                            params: {
                                mealId: itemData.item.id,
                            },
                        }
                    )
                }} />
        );
    };

    return (
        <View style={styles.mealList}>
            <FlatList data={props.listData} renderItem={renderMealItem} style={{width: '90%', marginTop: 20}} />
        </View>
    );
}

const styles = StyleSheet.create({
    mealList: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});